import {View, Text, StyleSheet, FlatList, TouchableOpacity} from 'react-native';
import React, {useState} from 'react';

export default function App() {
  const calculatorKeys = [
    {key: 'C'},
    {key: '%'},
    {key: '<'},
    {key: '/'},
    {key: '7'},
    {key: '8'},
    {key: '9'},
    {key: '*'},
    {key: '4'},
    {key: '5'},
    {key: '6'},
    {key: '-'},
    {key: '1'},
    {key: '2'},
    {key: '3'},
    {key: '+'},
    {key: '00'},
    {key: '0'},
    {key: '.'},
    {key: '='},
  ];
  const [displayValue, setDisplayValue] = useState('');
  const [history, setHistory] = useState([]);
  const [isShowHistory, setIsShowHistory] = useState(false); //TODO: Rename flag -> isShowHistory

  const operatorsList = ['+', '-', '*', '/', '%', '.'];
  const nonPrefixedKeys = ['*', '/', '%', '.', '='];

  const HkeyPressHolder = () => {
    setIsShowHistory(prevState => {
      return !prevState;
    });
  };

  const clearValues = () => {
    setDisplayValue('');
  };

  const pressHolder = key => {
    if (displayValue === '' && nonPrefixedKeys.includes(key)) {
      setDisplayValue('');
      return;
    }

    if (key === 'C') {
      clearValues();
      return;
    }
    if (key === '<') {
      setDisplayValue(prev => {
        prev = prev.slice(0, displayValue.length - 1);
        return prev;
      });
    } else if (key === '=') {
      if (displayValue === '') {
        setDisplayValue(displayValue);
      } else if (operatorsList.includes(displayValue.slice(-1))) {
        setDisplayValue(displayValue);
      } else {
        if (
          displayValue.includes('+') ||
          displayValue.includes('-') ||
          displayValue.includes('*') ||
          displayValue.includes('/') ||
          displayValue.includes('%')
        ) {
          // eslint-disable-next-line no-eval
          var displayValue1 = eval(displayValue).toString();
          setDisplayValue(displayValue1);
          setHistory(prevHistory => {
            prevHistory.push(displayValue + ' = ' + displayValue1 + '    ');
            return prevHistory;
          });
        }
      }
    } else {
      var value = displayValue.slice(-1);
      if (
        (value === '+' ||
          value === '-' ||
          value === '*' ||
          value === '/' ||
          value === '%' ||
          value == '.') &&
        (key == '+' ||
          key == '-' ||
          key == '*' ||
          key == '%' ||
          key == '/' ||
          key == '.')
      ) {
        setDisplayValue(displayValue);
      } else {
        if (displayValue.length < 40) {
          setDisplayValue(prev => {
            prev += key;
            return prev;
          });
        }
      }
    }
  };

  const renderKeys = ({item}) => (
    <View>
      <TouchableOpacity
        onPress={() => {
          pressHolder(item.key);
        }}>
        <Text style={styles.calKeys}>{item.key}</Text>
      </TouchableOpacity>
    </View>
  );

  return (
    <View style={{flex: 1}}>
      <View style={styles.display}>
        <Text style={styles.displayText}>{displayValue}</Text>
      </View>
      <View style={{flexDirection: 'row', borderWidth: 2}}>
        <View style={styles.historyBlock}>
          <TouchableOpacity
            onPress={() => {
              HkeyPressHolder();
            }}>
            <Text style={styles.HKey}>H</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.innerHistoryBlock}>
          <FlatList
            horizontal
            data={history}
            renderItem={({item}) =>
              isShowHistory ? (
                <Text style={styles.historyText}> {item} </Text>
              ) : (
                ''
              )
            }
          />
        </View>
      </View>
      <View style={styles.keysBlock}>
        <FlatList
          numColumns={4}
          data={calculatorKeys}
          renderItem={renderKeys}
          keyExtractor={({key}) => key}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  display: {
    flex: 40,
    marginTop: 10,
    borderWidth: 7,
  },
  calKeys: {
    marginHorizontal: 16,
    padding: 30,
    fontSize: 20,
    fontWeight: 'bold',
    color: 'black',
  },
  historyBlock: {
    marginTop: 5,
    padding: 20,
    borderWidth: 1,
    left: 5,
    bottom: 2,
  },
  HKey: {
    fontWeight: 'bold',
    fontSize: 28,
    color: 'black',
  },
  displayText: {
    textAlign: 'right',
    fontSize: 33,
    fontWeight: 'bold',
    color: 'black',
  },
  keysBlock: {
    borderWidth: 1,
    marginTop: 5,
  },
  innerHistoryBlock: {
    flex: 1,
    top: 25,
    left: 7,
  },
  historyText: {
    fontWeight: 'bold',
    fontSize: 18,
    color: 'black',
  },
});
